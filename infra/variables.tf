variable "oci_region" {
  type    = string
  default = "us-phoenix-1"
}

variable "oci_tenant_id" {
  type    = string
}

variable "oci_user_id" {
  type    = string
}

variable "oci_fingerprint" {
  type    = string
}

variable "oci_key_file" {
  type    = string
}

variable "cloudflare_api_token" {
  type      = string
  sensitive = true
}

variable "gitlab_prometheus_webhook_url" {
  type      = string
  sensitive = true
}

variable "gitlab_prometheus_auth_key" {
  type      = string
  sensitive = true
}

variable "pushover_user_key" {
  type      = string
  sensitive = true
}

variable "pushover_token" {
  type      = string
  sensitive = true
}
