terraform {
  backend "http" {}
  required_providers {
    oci = {
      source  = "oracle/oci"
      version = "4.120.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.9.0"
    }
  }
}

provider "oci" {
  tenancy_ocid     = var.oci_tenant_id
  user_ocid        = var.oci_user_id
  fingerprint      = var.oci_fingerprint
  private_key_path = var.oci_key_file
  region           = var.oci_region
}

provider "helm" {
  kubernetes {
    host                   = module.oke_cluster.api_endpoint
    cluster_ca_certificate = base64decode(module.oke_cluster.cluster_ca_certificate)
    exec {
      api_version = module.oke_cluster.exec_block.apiVersion
      command     = module.oke_cluster.exec_block.command
      args        = module.oke_cluster.exec_block.args
      env = {
        OCI_CLI_TENANCY     = var.oci_tenant_id
        OCI_CLI_USER        = var.oci_user_id
        OCI_CLI_FINGERPRINT = var.oci_fingerprint
        OCI_CLI_KEY_FILE    = var.oci_key_file
      }
    }
  }
}
