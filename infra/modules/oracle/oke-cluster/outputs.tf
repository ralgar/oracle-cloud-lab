data "oci_containerengine_cluster_kube_config" "k8s" {
  cluster_id = oci_containerengine_cluster.k8s.id
}

locals {
  kube_yaml    = yamldecode(data.oci_containerengine_cluster_kube_config.k8s.content)
  kube_cluster = local.kube_yaml.clusters[0].cluster
  kube_exec    = local.kube_yaml.users[0].user.exec

  api_endpoint           = local.kube_cluster.server
  cluster_ca_certificate = local.kube_cluster.certificate-authority-data
}

resource "local_sensitive_file" "kube_config" {
  filename = "${path.root}/../output/kube_config"
  content  = data.oci_containerengine_cluster_kube_config.k8s.content

  directory_permission = "0700"
  file_permission      = "0600"
}

output "api_endpoint" { value = local.api_endpoint }
output "cluster_ca_certificate" { value = local.cluster_ca_certificate }
output "exec_block" { value = local.kube_exec }
