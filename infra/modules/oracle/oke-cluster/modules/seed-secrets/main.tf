resource "helm_release" "seed_secrets" {
  chart   = "${path.module}/chart"
  version = "0.0.0"
  name    = "seed-secrets"
  atomic  = true

  set_sensitive {
    name  = "cloudflare_api_token"
    value = sensitive(var.cloudflare_api_token)
  }

  set_sensitive {
    name  = "gitlab_prometheus_webhook_url"
    value = sensitive(var.gitlab_prometheus_webhook_url)
  }

  set_sensitive {
    name  = "gitlab_prometheus_auth_key"
    value = sensitive(var.gitlab_prometheus_auth_key)
  }

  set_sensitive {
    name  = "pushover_user_key"
    value = sensitive(var.pushover_user_key)
  }

  set_sensitive {
    name  = "pushover_token"
    value = sensitive(var.pushover_token)
  }
}
