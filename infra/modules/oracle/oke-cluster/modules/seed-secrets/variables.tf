variable "cloudflare_api_token" {
  type      = string
  sensitive = true
}

variable "gitlab_prometheus_webhook_url" {
  type      = string
  sensitive = true
}

variable "gitlab_prometheus_auth_key" {
  type      = string
  sensitive = true
}

variable "pushover_user_key" {
  type      = string
  sensitive = true
}

variable "pushover_token" {
  type      = string
  sensitive = true
}
