variable "compartment_id" { type = string }
variable "vcn_id" { type = string }
variable "public_subnet_id" { type = string }
variable "private_subnet_id" { type = string }

variable "kubernetes_version" {
  type = string
}

variable "cloudflare_api_token" {
  type      = string
  sensitive = true
}

variable "gitlab_prometheus_webhook_url" {
  type      = string
  sensitive = true
}

variable "gitlab_prometheus_auth_key" {
  type      = string
  sensitive = true
}

variable "pushover_user_key" {
  type      = string
  sensitive = true
}

variable "pushover_token" {
  type      = string
  sensitive = true
}

variable "bootstrap" {
  description = "Toggles whether to bootstrap the cluster using Flux v2."
  type        = object({
    enabled    = bool
    repository = string
    branch     = string
    path       = string
  })
  default     = {
    enabled     = false
    repository  = null
    branch      = null
    path        = null
  }
}
