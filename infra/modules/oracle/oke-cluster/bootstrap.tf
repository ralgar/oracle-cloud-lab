module "flux" {
  source     = "./modules/flux"
  depends_on = [oci_containerengine_node_pool.arm64]
  count      = var.bootstrap.enabled ? 1 : 0

  repository = var.bootstrap.repository
  branch     = var.bootstrap.branch
  path       = var.bootstrap.path
}

module "seed_secrets" {
  source     = "./modules/seed-secrets"
  depends_on = [oci_containerengine_node_pool.arm64]
  count      = var.bootstrap.enabled ? 1 : 0

  cloudflare_api_token = var.cloudflare_api_token

  // Monitoring / Alerting
  gitlab_prometheus_webhook_url = var.gitlab_prometheus_webhook_url
  gitlab_prometheus_auth_key    = var.gitlab_prometheus_auth_key
  pushover_user_key             = var.pushover_user_key
  pushover_token                = var.pushover_token
}
