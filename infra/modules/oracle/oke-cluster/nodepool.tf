data "oci_identity_availability_domains" "ads" {
  compartment_id = var.compartment_id
}

locals {
  # Gather a list of availability domains for use in configuring placement_configs
  ads = data.oci_identity_availability_domains.ads.availability_domains[*].name
}

data "oci_containerengine_node_pool_option" "images" {
  node_pool_option_id = oci_containerengine_cluster.k8s.id
}

resource "oci_containerengine_node_pool" "arm64" {
  cluster_id         = oci_containerengine_cluster.k8s.id
  compartment_id     = var.compartment_id
  name               = "k8s-arm64_node_pool"
  node_shape         = "VM.Standard.A1.Flex"

  kubernetes_version = "v${var.kubernetes_version}"

  initial_node_labels {
    key   = "name"
    value = "k8s"
  }

  node_config_details {
    size = 2
    dynamic placement_configs {
      for_each = local.ads
      content {
        availability_domain = placement_configs.value
        subnet_id           = var.private_subnet_id
      }
    }
  }

  node_shape_config {
    memory_in_gbs = 12
    ocpus         = 2
  }

  node_source_details {
    image_id    = data.oci_containerengine_node_pool_option.images.sources[0].image_id
    source_type = "image"
  }
}
