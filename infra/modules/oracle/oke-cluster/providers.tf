terraform {
  required_providers {
    oci = { source = "oracle/oci" }
    local = { source = "hashicorp/local" }
  }
}
