module "internal_network" {
  source  = "./modules/oracle/network"

  compartment_id = var.oci_tenant_id
  region         = var.oci_region
}

module "oke_cluster" {
  source  = "./modules/oracle/oke-cluster"

  compartment_id = var.oci_tenant_id

  kubernetes_version = "1.26.2"

  // Networking
  vcn_id            = module.internal_network.vcn_id
  private_subnet_id = module.internal_network.private_subnet_id
  public_subnet_id  = module.internal_network.public_subnet_id

  // CertManager / ExternalDNS
  cloudflare_api_token = var.cloudflare_api_token

  // Monitoring / Alerting
  gitlab_prometheus_webhook_url = var.gitlab_prometheus_webhook_url
  gitlab_prometheus_auth_key    = var.gitlab_prometheus_auth_key
  pushover_user_key             = var.pushover_user_key
  pushover_token                = var.pushover_token

  // GitOps
  bootstrap = {
    enabled    = true
    repository = "https://gitlab.com/ralgar/oracle-cloud-lab.git"
    branch     = "main"
    path       = "./cluster/system/flux-sync"
  }
}
