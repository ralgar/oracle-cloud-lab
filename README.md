<!-- markdownlint-disable-next-line MD033 MD041 -->
<div align="center">

# Oracle Cloud Lab

[![Latest Tag](https://img.shields.io/gitlab/v/tag/ralgar/oracle-cloud-lab?style=flat&label=Tag&logo=semver&logoColor=white)](https://gitlab.com/ralgar/oracle-cloud-lab/tags)
[![Docs Status](https://img.shields.io/website?label=Docs&logo=gitbook&logoColor=white&style=flat&url=https%3A%2F%2Fdocs.ralgar.dev%2Fcategory/%2Foracle-cloud-lab%2F)](https://docs.ralgar.dev/category/oracle-cloud-lab/)
[![Pipeline Status](https://img.shields.io/gitlab/pipeline-status/ralgar/oracle-cloud-lab?branch=main&label=Pipeline&logo=gitlab&style=flat)](https://gitlab.com/ralgar/oracle-cloud-lab/-/pipelines?page=1&scope=all&ref=main)
[![Software License](https://img.shields.io/badge/License-BSD_2--clause-red?style=flat&logo=freebsd&logoColor=red)](https://choosealicense.com/licenses/bsd-2-clause/)
[![GitLab Stars](https://img.shields.io/gitlab/stars/ralgar/oracle-cloud-lab?color=gold&label=Stars&logo=gitlab&style=flat)](https://gitlab.com/ralgar/oracle-cloud-lab/-/starrers)

</div>

## Overview

By following the [GitOps](https://about.gitlab.com/topics/gitops) paradigm,
 this project is able to completely automate the deployment and operation of
 a **free** Kubernetes cluster on Oracle Cloud. I successfully used this to
 host my personal blog in production for several months, with zero downtime,
 before moving on to a more convenient hosting solution.

Check out the [documentation](https://docs.ralgar.dev/category/oracle-cloud-lab)
 for more information, and setup instructions.
